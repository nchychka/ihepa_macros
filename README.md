# IHEPA_macros

Examples of simple fits 

# MAXIMUM LIKELIHOOD ESTIMATION:

## MLE0 DATA POINT FITTING

Datapoint or several datapoints are generated. They/it follow/s the distribution: normal, poisson, gamma, or beta. 
This point is one of the inputs for generating a likelihood distribution. In case of generating several datapoints, the likelihood distributions are generated for each datapoint, then convoluted together to obtain an overall likelihood scan (green curve). The maximum value of a scan is the best parameter estimation.

## MLE1 FUNCTION WITH 2 PARAMETERS FITTING: LINE FUNCTION F(X) = A * X + B

A - slope, B - y-intercept.

Error calculation 1: Mean Squared Error is calculated - implicit MLE. 
Selection 1: The smallest MSE value is found by looping over calculated MSEs within the range.

## MLE1 FUNCTION WITH 2 PARAMETERS FITTING: POWER FUNCTION F(X) = X ** A * B

A - power, B - slope.

Error calculation 1: Mean Squared Error is calculated - implicit MLE. 
Selection 1: The smallest MSE value is found by looping over calculated MSEs within the range.

## MLE2 FUNCTION WITH 3 PARAMETERS: COSINE FUNCTION F(X) = A * COS(B * X + C) - TBD

A - amplitude, B - frequency, C - phase shift.

## SELECTION 2: GRADIENT DESCENT - TBD

## ERROR CALCULATION 2: CHI-SQUARED - TBD

## FITTING 1 HISTOGRAM - TBD

## FITTING 2 HISTOGRAMS - TBD


