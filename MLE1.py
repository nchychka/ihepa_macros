import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import yaml


def Calculate_mse(y_true, y_predicted):
    return np.mean((y_true - y_predicted) ** 2)


def Fitting_function_linear(x, params):
    a, b = params
    return x * a + b


def Fitting_function_power(x, params):
    a, b = params
    return x ** a * b


def Generate_line_points(num_points, x_range, y_range, line_params, NOISE_FACTOR):
    x_values = np.linspace(*x_range, num_points)
    y_error = np.random.normal(1, NOISE_FACTOR, size=num_points)
    y = np.add(np.multiply(x_values, line_params[0]), line_params[1])
    y_values = np.multiply(y, y_error)
    return x_values, y_values


def Generate_power_points(num_points, x_range, y_range, line_params, NOISE_FACTOR):
    x_values = np.linspace(*x_range, num_points)
    y_error = np.random.normal(1, NOISE_FACTOR, size=num_points)
    y = np.multiply(np.power(x_values, line_params[0]), line_params[1])
    y_values = np.multiply(y, y_error)
    return x_values, y_values


def Make_MSE_grid(x_values, y_values, NUM_BINS, SLOPE_RANGE, Y_INTERCEPT_RANGE, func):
    mse_values_linear = np.zeros((NUM_BINS, NUM_BINS))
    for i, slope in enumerate(np.linspace(*SLOPE_RANGE, NUM_BINS)):
        for j, y_intercept in enumerate(np.linspace(*Y_INTERCEPT_RANGE, NUM_BINS)):
            y_predicted = func(x_values, (slope, y_intercept))
            mse_values_linear[i, j] = Calculate_mse(y_values, y_predicted)
    return mse_values_linear


def Make_grid(NUM_BINS, SLOPE_RANGE, Y_INTERCEPT_RANGE):
    grid_x = np. zeros((NUM_BINS * NUM_BINS))
    grid_y = np. zeros((NUM_BINS * NUM_BINS))
    for i, slope in enumerate(np.linspace(*SLOPE_RANGE, NUM_BINS)):
        for j, y_intercept in enumerate(np.linspace(*Y_INTERCEPT_RANGE, NUM_BINS)):
            grid_x[i * NUM_BINS + j] = slope
            grid_y[i * NUM_BINS + j] = y_intercept
    return grid_x, grid_y


def Find_minimum_coords(mse_values_linear):
    min_mse_index_linear = np.argmin(mse_values_linear)
    i, j = np.unravel_index(min_mse_index_linear, mse_values_linear.shape)
    return i, j


def Get_slope_intercept(i, j, NUM_BINS, SLOPE_RANGE, Y_INTERCEPT_RANGE):
    a = np.linspace(*SLOPE_RANGE, NUM_BINS)[i]
    b = np.linspace(*Y_INTERCEPT_RANGE, NUM_BINS)[j]
    return a, b


def Plot_data_and_fit_2_params(x_values, y_values, fitting_params, fitting_function):
    plt.plot(x_values, y_values, 'o', label='Random Data Points')
    plt.plot(x_values, fitting_function(x_values, fitting_params), '-r', alpha=0.3,
             label=f'Best Fit: a = {fitting_params[0]:.2f}; b = {fitting_params[1]:.2f}')
             # y = a * x + b
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.title('Data with Best Fit (2 params)')
    plt.legend()
    plt.grid(True)


if __name__ == "__main__":
    cfg = None
    try:
        with open("MLE1.yaml", 'r') as stream:
            cfg = yaml.safe_load(stream)
    except FileNotFoundError:
        print("File not found. Please make sure the file 'MLE1.yaml' exists.")
    except yaml.YAMLError as exc:
        print("Error reading YAML file:", exc)


    # Generate random data
    x_values, y_values = Generate_line_points(cfg["NUM_POINTS"], cfg["X_RANGE"], cfg["Y_RANGE"], cfg["line_params"], cfg["NOISE_FACTOR"])
    mse_values_linear = Make_MSE_grid(x_values, y_values, cfg["NUM_BINS"], cfg["SLOPE_RANGE"], cfg["Y_INTERCEPT_RANGE"], Fitting_function_linear)
    i, j = Find_minimum_coords(mse_values_linear)
    a, b = Get_slope_intercept(i, j, cfg["NUM_BINS"], cfg["SLOPE_RANGE"], cfg["Y_INTERCEPT_RANGE"])


    # Plot
    fig = plt.figure(figsize=(15, 12))

    ax1 = fig.add_subplot(2, 2, 1)
    x, y = Make_grid(cfg["NUM_BINS"], cfg["SLOPE_RANGE"], cfg["Y_INTERCEPT_RANGE"])

    hist = ax1.hist2d(x, y,
            bins=cfg["NUM_BINS"], weights=mse_values_linear.ravel(), norm=mpl.colors.LogNorm(), cmap='viridis_r')
    plt.colorbar(hist[3], ax=ax1, label='MSE')
    ax1.set_xlabel('Slope')
    ax1.set_ylabel('Y-intercept')
    ax1.set_title('2D Histogram MSE values: Y = A * X + B')
    ax1.add_patch(plt.Rectangle((a, b),
                                np.diff(np.linspace(*cfg["SLOPE_RANGE"], cfg["NUM_BINS"]))[0],
                                np.diff(np.linspace(*cfg["Y_INTERCEPT_RANGE"], cfg["NUM_BINS"]))[0],
                                fill=False, edgecolor='red', lw=3))

    ax2 = fig.add_subplot(2, 2, 2)
    Plot_data_and_fit_2_params(x_values, y_values, (a, b), Fitting_function_linear)


    x_values2, y_values2 = Generate_power_points(cfg["NUM_POINTS"], cfg["X_RANGE"], cfg["Y_RANGE"], cfg["power_params"], cfg["NOISE_FACTOR"])
    mse_values_power = Make_MSE_grid(x_values2, y_values2, cfg["NUM_BINS"], cfg["SLOPE_RANGE"], cfg["POWER_RANGE"], Fitting_function_power)
    i2, j2 = Find_minimum_coords(mse_values_power)
    a2, b2 = Get_slope_intercept(i2, j2, cfg["NUM_BINS"], cfg["SLOPE_RANGE"], cfg["POWER_RANGE"])

    ax3 = fig.add_subplot(2, 2, 3)
    x2, y2 = Make_grid(cfg["NUM_BINS"], cfg["SLOPE_RANGE"], cfg["POWER_RANGE"])

    hist2 = ax3.hist2d(x2, y2,
            bins=cfg["NUM_BINS"], weights=mse_values_power.ravel(), norm=mpl.colors.LogNorm(), cmap='viridis_r')
    plt.colorbar(hist2[3], ax=ax3, label='MSE')
    ax3.set_xlabel('Power')
    ax3.set_ylabel('Slope')
    ax3.set_title('2D Histogram MSE values: Y = X ** A * B')
    ax3.add_patch(plt.Rectangle((a2, b2),
                                np.diff(np.linspace(*cfg["SLOPE_RANGE"], cfg["NUM_BINS"]))[0],
                                np.diff(np.linspace(*cfg["POWER_RANGE"], cfg["NUM_BINS"]))[0],
                                fill=False, edgecolor='red', lw=3))

    ax4 = fig.add_subplot(2, 2, 4)
    Plot_data_and_fit_2_params(x_values2, y_values2, (a2, b2), Fitting_function_power)


    plt.tight_layout()
    plt.show()
