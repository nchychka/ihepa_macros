import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import yaml


class Empty_container:
    pass


def Generate_data(cfg):
    dist = cfg['generation']['data_source_dist']
    parameter_1 = cfg['generation']['p1']
    parameter_2 = cfg['generation']['p2']
    dim = cfg['dimensions']
    if 'normal' == dist:
        return np.random.normal(parameter_1, parameter_2, dim)
    if 'poisson' == dist: # pmf
        return np.random.poisson(parameter_1, dim)
    if 'gamma' == dist:
        return np.random.gamma(parameter_1, 1., dim) # match stats.gamma
    if 'beta' == dist:
        return np.random.beta(parameter_1, parameter_2, dim)

    raise ValueError('Not implemented data_source_dist: {}'.format(dist))


def Initializing_plots(cfg):
    plt.rcParams['text.usetex'] = True

    plots = Empty_container()
    # Create figure and axes objects
    plots.fig, (plots.ax1, plots.ax2) = plt.subplots(1, 2, figsize=(12, 5))

    return plots


def Add_data_lines_plotting_dist_and_data(plots, x_points, dim):
    ymin, ymax = plots.ax1.get_ylim()
    for i in range(dim):
        if i == 0:
            plots.ax1.plot([x_points[i], x_points[i]], [ymin, ymax], color='black',
                           linestyle='--', linewidth=2, label='Observed')
        else:
            plots.ax1.plot([x_points[i], x_points[i]], [ymin, ymax], color='black',
                           linestyle='--', linewidth=2)  # Changed linestyle to ' '



def Add_distribution_plotting_dist_and_data(plots, cfg):
    x = np.linspace(*cfg['data_range'], cfg['study_points'])
    y = None
    dist = cfg['generation']['data_source_dist']
    parameter_1 = cfg['generation']['p1']
    parameter_2 = cfg['generation']['p2']
    if 'normal' == dist:
        y = stats.norm.pdf(x, parameter_1, parameter_2)
    if 'poisson' == dist:
        y = stats.poisson.pmf(x, parameter_1)
    if 'gamma' == dist:
        y = stats.gamma.pdf(x, parameter_1, 1.) # match stats.gamma
    if 'beta' == dist:
        y = stats.beta.pdf(x, parameter_1, parameter_2)

    plots.ax1.plot(x, y, color='blue', label='Underlying distribution')


def Add_labels_plotting_dist_and_data(plots, x_points, cfg):
    # add explanation
    if cfg['dimensions'] == 1:
        plots.ax1.plot([], [], ' ',
                       label=r'True $p_1 = ${}, obs$=${:.2}'.format(cfg['generation']['p1'],
                                                                    x_points[0]))
    else:
        plots.ax1.plot([], [], ' ',
                       label=r'True $p_1 = ${}'.format(cfg['generation']['p1']))


    plots.ax1.set_xlabel('X')
    plots.ax1.set_ylabel('Probability density')
    plots.ax1.set_title('Underlying distribution with observed data')
    plots.ax1.set_xlim(cfg['data_range'])
    plots.ax1.grid(True)
    plots.ax1.legend()


def Likelihood_scan(x_points, scan_pa_vals, cfg):
    log_l = cfg['likelihood']['log-likelihood']
    likelihood_values = None
    if log_l:
        likelihood_values = np.zeros(cfg['study_points']) # initial 0s for add
    else:
        likelihood_values = np.ones(cfg['study_points']) # initial 1s for multiply

    dist = cfg['likelihood']['dist']
    parameter_2 = cfg['likelihood']['nominal_p2']

    for i in range(cfg['dimensions']):
        if 'normal' == dist:
            tmp = stats.norm.pdf(x_points[i],
                                 scan_pa_vals,      # mu
                                 parameter_2) # sigma
        if 'poisson' == dist:
            tmp = stats.poisson.pmf(x_points[i],
                                    scan_pa_vals)   # lambda
        if 'gamma' == dist:
            tmp = stats.gamma.pdf(x_points[i],
                                 scan_pa_vals)      # alpha
        if 'beta' == dist:
            tmp = stats.beta.pdf(x_points[i],
                                 scan_pa_vals,      # a
                                 parameter_2)  # b
        if log_l:
            tmp = np.log(tmp)
            likelihood_values = np.add(likelihood_values, tmp)
        else:
            likelihood_values = np.multiply(likelihood_values, tmp)

    return likelihood_values


def Plotting_likelihood_scan(plots, x_points, cfg):
    scan_pa_vals = np.linspace(*cfg['study_range'], cfg['study_points'])
    likelihood_values = Likelihood_scan(x_points, scan_pa_vals, cfg)

    # Maximum likelihood
    max_val_index = np.argmax(likelihood_values)
    max_likelihood_pa = scan_pa_vals[max_val_index]

    # plot likelihood scan
    plots.ax2.plot(scan_pa_vals, likelihood_values, marker='o',
                   linestyle='--', linewidth=1, color='green', label='Likelihood values')

    # plot best value
    ymin, ymax = plots.ax2.get_ylim()
    plots.ax2.plot([max_likelihood_pa, max_likelihood_pa], [ymin, ymax],
                   color='purple', linestyle='--', linewidth=3, label='Found maximum')
    # add explanation
    plots.ax2.plot([], [], ' ',
                   label=r'Max $L(p_1)$ at $p_1 = ${:.2}'.format(max_likelihood_pa))


def Add_labels_likelihood_scan(plots, cfg):
    plots.ax2.set_xlabel(r'$p_1$')
    if cfg['likelihood']['log-likelihood']:
        plots.ax2.set_ylabel(r'Log-Likelihood($p_1$)')
    else:
        plots.ax2.set_ylabel(r'Likelihood($p_1$)')
    plots.ax2.set_title('Likelihood scan')
    plots.ax2.set_xlim(cfg['study_range'])
    plots.ax2.grid(True)
    plots.ax2.legend()


if __name__ == "__main__":
    cfg = None
    try:
        with open("MLEO.yaml", 'r') as stream:
            cfg = yaml.safe_load(stream)
    except FileNotFoundError:
        print("File not found. Please make sure the file 'MLE0.yaml' exists.")
    except yaml.YAMLError as exc:
        print("Error reading YAML file:", exc)

    x_points = Generate_data(cfg)
    plots = Initializing_plots(cfg)

    Add_distribution_plotting_dist_and_data(plots, cfg)
    Add_data_lines_plotting_dist_and_data(plots, x_points, cfg['dimensions'])
    Add_labels_plotting_dist_and_data(plots, x_points, cfg)

    Plotting_likelihood_scan(plots, x_points, cfg)
    Add_labels_likelihood_scan(plots, cfg)

    # Adjust layout
    plt.tight_layout()

    # Show plot
    plt.show()

